add_executable(${PROJECT_NAME} main.cc)
target_link_libraries(${PROJECT_NAME}
  PRIVATE
    Boost::boost)
target_include_directories(${PROJECT_NAME}
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
