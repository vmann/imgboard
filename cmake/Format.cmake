file(GLOB_RECURSE all_sources
  RELATIVE "${PROJECT_SOURCE_DIR}"
  include/*.h src/*.cc src/*.h)
add_custom_target(${PROJECT_NAME}_format
  COMMENT "Formating ${PROJECT_NAME} with clang-format"
  COMMAND clang-format -style=file -i -- ${all_sources} VERBATIM
  WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}")
unset(all_sources)

if(NOT TARGET format)
  add_custom_target(format)
endif()
add_dependencies(format ${PROJECT_NAME}_format)
